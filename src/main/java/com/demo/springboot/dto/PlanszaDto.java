package com.demo.springboot.dto;


import java.util.HashMap;
import java.util.List;

public class PlanszaDto {
    private HashMap<Integer, String> plansza;

    public PlanszaDto(HashMap<Integer, String> plansza) {
        this.plansza = plansza;
    }

    public HashMap<Integer, String> getPlansza() {
        return plansza;
    }

    public void setPlansza(HashMap<Integer, String> plansza) {
        this.plansza = plansza;
    }

    @Override
    public String toString() {
        return "PlanszaDto{" +
                "plansza=" + plansza +
                '}';
    }
}
