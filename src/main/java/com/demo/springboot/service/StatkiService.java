package com.demo.springboot.service;

import com.demo.springboot.dto.PlanszaDto;

import java.util.Optional;

public interface StatkiService {
    PlanszaDto getPlansza();
    Optional<Boolean> shot(int position);
}