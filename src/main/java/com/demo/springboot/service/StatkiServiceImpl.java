package com.demo.springboot.service;

import com.demo.springboot.dto.PlanszaDto;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class StatkiServiceImpl implements StatkiService{

    private final int SIZE = 8;

    private PlanszaDto planszaDto;

    public StatkiServiceImpl() {
        HashMap<Integer, String> ships = new HashMap<>();   //("","S","","","","S","","S");
        ships.put(0, " ");
        ships.put(1, "S");
        ships.put(2, " ");
        ships.put(3, " ");
        ships.put(4, " ");
        ships.put(5, "S");
        ships.put(6, " ");
        ships.put(7, "S");
        planszaDto = new PlanszaDto(ships);
    }

    @Override
    public PlanszaDto getPlansza() {
        return planszaDto;
    }

    @Override
    public Optional<Boolean> shot(int position) {
        if(position >= planszaDto.getPlansza().size()  || position < 0 || planszaDto.getPlansza().get(position).equals("X") || planszaDto.getPlansza().get(position).equals("O")){
            return Optional.empty();
        }
        if(planszaDto.getPlansza().get(position).equals("S")){  // [] / set.() / replace()
            planszaDto.getPlansza().replace(position,"X");  // [] / set.() / replace()
            return Optional.of(true);
        }
        planszaDto.getPlansza().replace(position,"O"); // [] / set.() / replace()
        return Optional.of(false);
    }
}