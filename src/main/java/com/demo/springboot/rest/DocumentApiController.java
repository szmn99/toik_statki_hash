package com.demo.springboot.rest;

import com.demo.springboot.service.StatkiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class DocumentApiController {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentApiController.class);

    @Autowired
    StatkiService statkiService;

    @PostMapping("/api/shot")
    ResponseEntity<Void> shot(@RequestParam("position") String position){                                               //requestbody positionDto positondto
        Optional<Boolean> result = statkiService.shot(Integer.parseInt(position));                                      //shot(position.getpostionn)
        LOG.info(statkiService.getPlansza().toString());
        if(result.isPresent()){
            if(result.get()){
                return ResponseEntity.ok().build();
            }else{
                return ResponseEntity.notFound().build();
            }
        }else{
            return ResponseEntity.badRequest().build();
        }
    }
}

